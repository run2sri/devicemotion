//
//  main.m
//  DeviceMotion
//
//  Created by srbaskar on 10/13/2015.
//  Copyright (c) 2015 srbaskar. All rights reserved.
//

@import UIKit;
#import "DMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMAppDelegate class]));
    }
}
