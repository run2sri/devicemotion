//
//  DMViewController.m
//  DeviceMotion
//
//  Created by srbaskar on 10/13/2015.
//  Copyright (c) 2015 srbaskar. All rights reserved.
//

#import "DMViewController.h"
@import DeviceMotion;

double currentMaxAccelX;
double currentMaxAccelY;
double currentMaxAccelZ;
double currentMaxRotX;
double currentMaxRotY;
double currentMaxRotZ;

@interface DMViewController ()
@property (strong, nonatomic) IBOutlet UILabel *accX;
@property (strong, nonatomic) IBOutlet UILabel *accY;
@property (strong, nonatomic) IBOutlet UILabel *accZ;

@property (strong, nonatomic) IBOutlet UILabel *accl;
@property (strong, nonatomic) IBOutlet UILabel *maxaccl;
@property (strong, nonatomic) IBOutlet UILabel *rot;
@property (strong, nonatomic) IBOutlet UILabel *maxrot;

@property (strong, nonatomic) IBOutlet UILabel *maxAccX;
@property (strong, nonatomic) IBOutlet UILabel *maxAccY;
@property (strong, nonatomic) IBOutlet UILabel *maxAccZ;

@property (strong, nonatomic) IBOutlet UILabel *rotX;
@property (strong, nonatomic) IBOutlet UILabel *rotY;
@property (strong, nonatomic) IBOutlet UILabel *rotZ;

@property (strong, nonatomic) IBOutlet UILabel *maxRotX;
@property (strong, nonatomic) IBOutlet UILabel *maxRotY;
@property (strong, nonatomic) IBOutlet UILabel *maxRotZ;


@end

@implementation DMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    DeviceMotionMager *dm = [[DeviceMotionMager alloc] init];
    [dm initWithAccelerometerUpdatesWithHandler:^(CMAcceleration acceleration) {
        [self outputAccelertionData:acceleration];
    }];
    
    [dm initWithGyroUpdatesWithHandler:^(CMRotationRate rotation) {
        [self outputRotationData:rotation];
    }];
}

-(void)outputAccelertionData:(CMAcceleration)acceleration
{
    NSString *accX = [NSString stringWithFormat:@" %.2fg",acceleration.x];
    if(fabs(acceleration.x) > fabs(currentMaxAccelX))
    {
        currentMaxAccelX = acceleration.x;
    }
    NSString *accY = [NSString stringWithFormat:@" %.2fg",acceleration.y];
    if(fabs(acceleration.y) > fabs(currentMaxAccelY))
    {
        currentMaxAccelY = acceleration.y;
    }
    NSString *accZ = [NSString stringWithFormat:@" %.2fg",acceleration.z];
    if(fabs(acceleration.z) > fabs(currentMaxAccelZ))
    {
        currentMaxAccelZ = acceleration.z;
    }
    
    self.accl.text = [NSString stringWithFormat:@"X:%@  Y:%@  Z:%@", accX, accY, accZ];

    NSString *maxAccX = [NSString stringWithFormat:@" %.2f",currentMaxAccelX];
    NSString *maxAccY = [NSString stringWithFormat:@" %.2f",currentMaxAccelY];
    NSString *maxAccZ = [NSString stringWithFormat:@" %.2f",currentMaxAccelZ];

    self.maxaccl.text = [NSString stringWithFormat:@"MAXX:%@ MAXY:%@ MAXZ:%@", maxAccX, maxAccY, maxAccZ];
}
-(void)outputRotationData:(CMRotationRate)rotation
{
    NSString *rotX = [NSString stringWithFormat:@" %.2fr/s",rotation.x];
    if(fabs(rotation.x) > fabs(currentMaxRotX))
    {
        currentMaxRotX = rotation.x;
    }
    NSString *rotY = [NSString stringWithFormat:@" %.2fr/s",rotation.y];
    if(fabs(rotation.y) > fabs(currentMaxRotY))
    {
        currentMaxRotY = rotation.y;
    }
    NSString *rotZ = [NSString stringWithFormat:@" %.2fr/s",rotation.z];
    if(fabs(rotation.z) > fabs(currentMaxRotZ))
    {
        currentMaxRotZ = rotation.z;
    }
    
    NSString *maxRotX = [NSString stringWithFormat:@" %.2f",currentMaxRotX];
    NSString *maxRotY = [NSString stringWithFormat:@" %.2f",currentMaxRotY];
    NSString *maxRotZ = [NSString stringWithFormat:@" %.2f",currentMaxRotZ];
    
    self.accl.text = [NSString stringWithFormat:@"X:%@  Y:%@  Z:%@", rotX, rotY, rotZ];
    self.maxaccl.text = [NSString stringWithFormat:@"MAXX:%@ MAXY:%@ MAXZ:%@", maxRotX, maxRotY, maxRotZ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
