//
//  DMAppDelegate.h
//  DeviceMotion
//
//  Created by srbaskar on 10/13/2015.
//  Copyright (c) 2015 srbaskar. All rights reserved.
//

@import UIKit;

@interface DMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
