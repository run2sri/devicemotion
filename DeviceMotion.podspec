#
# Be sure to run `pod lib lint DeviceMotion.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "DeviceMotion"
  s.version          = "0.1.0"
  s.summary          = "Get Coordinates using Core Motion to Access Gyro and Accelerometer."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                        One of the coolest things smartphones are capable of now is the ability to sense motion and orientation. The iPhone does this with the use of a 3-axis Accelerometers and Gyroscopes. In this tutorial we’re going to create an app that indicates the current G and speed of rotation. Not exactly a pretty app but still instructional.
                       DESC

  s.homepage         = "https://bitbucket.org/run2sri/devicemotion"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "run2sri" => "run2sri@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/run2sri/devicemotion", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'DeviceMotion' => ['Pod/Assets/*.png']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
